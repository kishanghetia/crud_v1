import React, { useState, createContext } from "react";
// let AppDataState: { id: number; name: string }[] = [];
type DataObjType = {
  id: number | undefined;
  description: string;
  shortCutkey: string;
};
type AppDataType = {
  DataObj: Array<DataObjType> | null;
  HandleData: React.Dispatch<React.SetStateAction<Array<DataObjType> | null>>;
};

type appContextProviderProps = {
  children: React.ReactNode;
};

export const Datacontext = createContext({} as AppDataType);
export const DataContextProvider = ({ children }: appContextProviderProps) => {
  const [DataObj, HandleData] = useState<Array<DataObjType> | null>([
    { id:1,description: "copy text", shortCutkey: "ctrl + c" },
    { id:2, description: "paste text", shortCutkey: "ctrl + v" },
  ]);
  return (
    <Datacontext.Provider value={{ DataObj, HandleData }}>
      {children}
    </Datacontext.Provider>
  );
};

export default DataContextProvider;
