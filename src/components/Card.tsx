import React, { useContext } from "react";
import { Datacontext } from "./DataContext";

type cardProps = {
  id: number | undefined;
  description: string;
  shortCutkey: string;
};
type data = {
  cardata: cardProps;
};
function Card(props: data) {
  const { id, description, shortCutkey } = props.cardata;
  const dataContext = useContext(Datacontext);

  const deleteCard = (e: React.MouseEvent<HTMLElement>) => {
    console.log(id, dataContext.DataObj);
    let index = id! -1
    let tempData = [...dataContext.DataObj!];
    tempData!.splice(index, 1);
    dataContext.HandleData([...tempData]);
  };

  return (
    <div className="flex rounded overflow-hidden shadow-lg w-auto m-2 p-3 border-blue-300	border">
      <div className="flex flex-col justify-around	text-left	mr-5">
        <h3 className="text-2xl font-bold ">{description}</h3>
        <p className="bg-lime-300 p-2 text-center rounded">{shortCutkey}</p>
      </div>
      <div className="flex flex-col">
        <button
          type="button"
          className="bg-blue-500 hover:bg-blue-400	border-blue-700 hover:border-blue-500 border-b-4 rounded p-2 m-1"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
          </svg>
        </button>
        <button
          type="button"
          className="bg-red-500 hover:bg-red-400	border-red-700 hover:border-red-500 border-b-4 rounded p-2 m-1"
          onClick={deleteCard}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </div>
    </div>
  );
}

export default Card;
