import React, { useState, createContext } from "react";

type cardType = {
  description: string;
  shortCutkey: string;
};

type shortcutCard = {
  shortcut: cardType | null;
  handleShortCutInput: React.Dispatch<React.SetStateAction<cardType | null>>;
};

type appContextProviderProps = {
  children: React.ReactNode;
};
export const CardContext = createContext({} as shortcutCard);

export const AppContextProvider = ({ children }: appContextProviderProps) => {
  const [shortcut, handleShortCutInput] = useState<cardType | null>(null);
  return (
    <CardContext.Provider value={{ shortcut, handleShortCutInput }}>
      {children}
    </CardContext.Provider>
  );
};

export default AppContextProvider;
