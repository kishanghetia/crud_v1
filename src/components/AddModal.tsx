import React, { useContext } from "react";
import { CardContext } from "./context";
import { Datacontext } from "./DataContext";

function AddModal() {
  const cardContext = useContext(CardContext);
  const dataContext = useContext(Datacontext);

  const toggleModal = () => {
    let modalId = document.getElementById("modal")!;
    modalId.classList.toggle("hidden");
  };

  const handleDesInput = (e: React.FormEvent<HTMLInputElement>): void => {
    cardContext.handleShortCutInput({
      ...cardContext.shortcut!,
      description: e.currentTarget.value,
    });
  };
  const handleShortCutInput = (e: React.FormEvent<HTMLInputElement>): void => {
    cardContext.handleShortCutInput({
      ...cardContext.shortcut!,
      shortCutkey: e.currentTarget.value,
    });
  };
  const handlesubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    if (dataContext.DataObj !== null) {
      let tempData = [...dataContext.DataObj!];
      let newid = tempData.length + 1;
      let newData = { ...cardContext.shortcut!, id: newid };
      tempData = [...tempData, newData];
      dataContext.HandleData([...tempData]);
    } else {
      let tempData = [...dataContext.DataObj!];
      let newid = tempData.length + 1;
      let newData = { ...cardContext.shortcut!, id: newid };
      tempData = [...tempData, newData];
      dataContext.HandleData([...tempData]);
    }
    cardContext.handleShortCutInput({
      ...cardContext.shortcut!,
      shortCutkey: "",
      description: "",
    });
  };

  return (
    <div className="ml-auto">
      <div className="flex items-center justify-center">
        <button
          className=" flex items-center bg-blue-500 hover:bg-blue-400 text-white h-10
          font-bold py-2 px-4 ml-auto border-b-4 border-blue-700 hover:border-blue-500
          rounded"
          onClick={toggleModal}
        >
          Add ShorCut
        </button>
      </div>
      <div
        className="fixed z-10 overflow-y-auto top-0 w-full left-0 hidden"
        id="modal"
      >
        <div className="flex items-center justify-center min-height-100vh pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <div className="fixed inset-0 transition-opacity">
            <div className="absolute inset-0 bg-gray-900 opacity-75" />
          </div>
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen">
            &#8203;
          </span>
          <div
            className="inline-block align-center bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle "
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-headline"
          >
            <form onSubmit={handlesubmit}>
              <div className="flex p-4 items-center justify-center">
                <p>Description</p>
                <input
                  type="text"
                  className="outline-none border border-gray-400 rounded py-2 px-2 mx-2 bg-white text-sm text-gray-700 placeholder-gray-400 focus:outline-none focus:shadow-outline"
                  name="Description"
                  value={cardContext.shortcut?.description}
                  onChange={handleDesInput}
                ></input>
                <p>Short cmd key</p>
                <input
                  type="text"
                  className="outline-none border border-gray-400 rounded py-2 px-2 mx-2 bg-white text-sm text-gray-700 placeholder-gray-400 focus:outline-none focus:shadow-outline"
                  name="ShortCut"
                  value={cardContext.shortcut?.shortCutkey}
                  onChange={handleShortCutInput}
                ></input>
              </div>
              <div className="bg-gray-200 px-4 py-3 text-right ">
                <button
                  type="button"
                  className="py-2 px-4 bg-gray-500 text-white rounded hover:bg-gray-700 mr-2"
                  onClick={toggleModal}
                >
                  <i className="fas fa-times"></i> close
                </button>
                <button
                  type="submit"
                  className="py-2 px-4 bg-blue-500 text-white rounded hover:bg-blue-700 mr-2"
                >
                  <i className="fas fa-plus"></i> Save
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddModal;
