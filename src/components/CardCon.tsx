import React, { useContext } from "react";
import Card from "./Card";
import { Datacontext } from "./DataContext";

function Header() {
  const dataContext = useContext(Datacontext);
  let dataArr = [...dataContext.DataObj!];
  // console.log("here is data array", dataArr);
  return (
    <div className="flex flex-wrap">
      {dataArr.map((data, index) => (
        <Card key={index} cardata={data} />
      ))}
    </div>
  );
}

export default Header;
