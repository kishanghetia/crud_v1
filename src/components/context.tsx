import React, { useState, createContext } from "react";

type cardType = {
  id:number | undefined,
  description: string;
  shortCutkey: string;
};
type shortcutCard = {
  shortcut: cardType;
  handleShortCutInput: React.Dispatch<React.SetStateAction<cardType>>;
};

type appContextProviderProps = {
  children: React.ReactNode;
};

export const CardContext = createContext({} as shortcutCard);
export const AppContextProvider = ({ children }: appContextProviderProps) => {

  const [shortcut, handleShortCutInput] = useState<cardType>({
    id:undefined,
    description: "",
    shortCutkey: "",
  });
  
  return (
    <CardContext.Provider value={{ shortcut, handleShortCutInput }}>
      {children}
    </CardContext.Provider>
  );
};

export default AppContextProvider;
