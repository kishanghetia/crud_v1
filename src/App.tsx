import React from "react";
import "./App.css";
import CardCon from "./components/CardCon";
import AddModal from "./components/AddModal";
import AppContextProvider from "./components/context";
import DataContainer from "./components/DataContext";

function App() {
  return (
    <div className="App h-screen flex flex-col">
      <DataContainer>
        <AppContextProvider>
          <div className="flex items-center bg-lime-300 text-gray-800	h-16 content-center px-4">
            <h1 className="text-3xl font-bold align-text-top flex items-center">
              Master Shortcuta
            </h1>
            <AddModal />
          </div>
          <div className="text-gray-800	bg-slate-100	p-9 flex-grow">
            <CardCon />
          </div>
        </AppContextProvider>
      </DataContainer>
    </div>
  );
}

export default App;
